import React from "react";
import Card from "./card";

const CardList = ({ book, search }) => {
  if (book == "") {
    return (
      <section className="booklist">
        <div className="container">
          <div className="section-title">
            <h2>{search} Books</h2>
          </div>
          <div className="booklist-content grid">
            <h1>No available Books</h1>
          </div>
        </div>
      </section>
    );
  } else {
    return (
      <section className="booklist">
        <div className="container">
          <div className="section-title">
            <h2>{search} Books</h2>
          </div>
          <div className="booklist-content grid">
            {book.map((item) => {
              let nothing = '';
              let thumbnail = item.volumeInfo.imageLinks.smallThumbnail;
              let publishedDate = item.volumeInfo.publishedDate;
              let publisherName = item.volumeInfo.publisher;
              let rating = item.volumeInfo.averageRating;
              let ratingCount = item.volumeInfo.ratingsCount;
              let authors = item.volumeInfo.authors;
              let isbn =
                item.volumeInfo.industryIdentifiers[0].identifier == undefined
                  ? item.volumeInfo.industryIdentifiers[1].identifier
                  : item.volumeInfo.industryIdentifiers[0].identifier;

              let id = item.id;
              let downloadLink = item.accessInfo.epub.acsTokenLink;

              return (
                <Card
                  key={id}
                  thumbnail={thumbnail}
                  publishedDate={publishedDate}
                  publisherName={publisherName}
                  rating={rating}
                  ratingCount={ratingCount}
                  authors={authors}
                  isbn={isbn}
                  downloadLink={downloadLink}
                />
              );
            })}
          </div>
        </div>
      </section>
    );
  }
};

export default CardList;
